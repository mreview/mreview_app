export const setToken = (taketoken) => ({
    type: "SET_TOKEN",
    token: taketoken
})
