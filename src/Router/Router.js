import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import TabBar from './TabBar';
import VideoPlayerView from '../Component/VideoPlayerView';
import Home from '../Component/Home';
import Recommend from '../Component/Recommend';
import Moive_Detail from '../Component/Movie_Detail';
import Movie_List from '../Component/Movie_List';
import Movie from '../Component/Movie';
import Profile from '../Component/Profile';
import User_Review from '../Component/User_Review';
import Write_Review from '../Component/Write_Review';
import Login from '../Component/Login-Register/Login';
import Register from '../Component/Login-Register/Register';
import News_Detail from '../Component/News_Detail';
import Photo_List from '../Component/Photo_List';
import Photo_Show from '../Component/Photo_Show';
import { loginReducer } from '../reducer/loginReducer';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import * as eva from '@eva-design/eva';
import { ApplicationProvider } from '@ui-kitten/components';



const store = createStore(loginReducer)

const Tab = createMaterialTopTabNavigator();


const Stack = createStackNavigator();

export default function Router() {
    return (
        <Provider store={store}>
            <ApplicationProvider {...eva} theme={eva.light}>
                <NavigationContainer>
                    <Stack.Navigator screenOptions={{ headerShown: false }} >
                        <Tab.Screen name="Home" component={Home} />
                        <Tab.Screen name="Video" component={VideoPlayerView} />
                        <Tab.Screen name="Movie" component={Movie} />
                        <Tab.Screen name="Recommend" component={Recommend} />
                        <Tab.Screen name="Movie_Detail" component={Moive_Detail} />
                        <Tab.Screen name="Movie_List" component={Movie_List} />
                        <Tab.Screen name="Profile" component={Profile} />
                        <Tab.Screen name="User_Review" component={User_Review} />
                        <Tab.Screen name="Write_Review" component={Write_Review} />
                        <Tab.Screen name="Login" component={Login} />
                        <Tab.Screen name="Register" component={Register} />
                        <Tab.Screen name="News_Detail" component={News_Detail} />
                        <Tab.Screen name="Photo_List" component={Photo_List} />
                        <Tab.Screen name="Photo_Show" component={Photo_Show} />
                    </Stack.Navigator>
                </NavigationContainer>
            </ApplicationProvider>
        </Provider>



    )
}