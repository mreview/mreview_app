// const getToken = () => {
//     return localStorage.getItem("token") || ""
// }


const initialState = {
    token: "",
}

export const loginReducer = (state = initialState, action) => {

    switch (action.type) {
        case 'SET_TOKEN':
            return {
                ...initialState,
                token: action.token,
            }
        case 'SET_USER':
            return {
                ...initialState,
                user: action.user,
            }


        default:
            return initialState
    }

}