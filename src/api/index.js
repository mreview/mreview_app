import axios from "axios";

// export const host = "http://192.168.1.6:8080";

// export const host = "http://10.80.9.111:8080";

export const host = "https://mreviewdbv2.jumpingcrab.com";



//------------------ User----------------

export function register(username, email, password) {
    return axios.post(`${host}/register`, username, email, password)
}

export function login(username, password) {
    return axios.post(`${host}/login`, username, password)
}

//------------------- News --------------------

export function listAllNews() {
    return axios.get(`${host}/news`)
}

export function listAllMovie() {
    return axios.get(`${host}/movie`)
}

//-----------------Movie ------------------

export function movieDetailList(id) {
    return axios.get(`${host}/movie/${id}`)
}

//-----------------Images --------------------

export function movieImages(id) {
    return axios.get(`${host}/movie/${id}/images`)
}

export function GetOneImages(id) {
    return axios.get(`${host}/image/${id}`)
}



//--------------- Catagories --------------

export function listCatagories() {
    return axios.get(`${host}/catagories`)
}

export function listCatagoriesMovie(id) {
    return axios.get(`${host}/catagories/${id}/movie`)
}


//----------------- Genres ----------------------
export function showGenres(id) {
    return axios.get(`${host}/movie/${id}/detail`)
}


//----------------- Review List ----------------
export function haveUserReview(id) {
    return axios.get(`${host}/movie/${id}/userreview`)
}

export function listGenres() {
    return axios.get(`${host}/ganres`)
}


export function CreateReview(id, { review_header, review_text }, token) {
    return axios.post(`${host}/auth/reviewmovie/${id}`, { review_header, review_text }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}


export function CreateRate(id, rate_type, rate_score, token) {
    return axios.post(`${host}/auth/reviewmovie/${id}/rate`, { rate_type: rate_type, rate_score: rate_score }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}


export function showReview(id) {
    return axios.get(`${host}/movie/${id}/review`)
}

export function showRateAllMovie(id) {
    return axios.get(`${host}/movie/${id}/rate`)
}

export function showRateReview(id) {
    return axios.get(`${host}/reviewmovie/${id}/rate`)
}


