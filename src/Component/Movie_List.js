import React, { useState, useEffect } from 'react'
import { View, ScrollView, TouchableOpacity, Text, SafeAreaView, FlatList, StyleSheet, Image } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { Card } from '@ant-design/react-native';
import { listCatagoriesMovie, showRateAllMovie } from '../api';


export default function Movie_List(props) {

    const movieList = props.route.params.item

    console.log(movieList);

    const [listMovie, setListMovie] = useState([]);
    const [allrate, setAllrate] = useState([]);
    const [allData, setAllData] = useState([]);
    var avg = 0;

    console.log("list Movie", listMovie);



    useEffect(() => {
        listShoeMovie()
        ListRateAllMovie()
    }, []);

    const listShoeMovie = () => {
        listCatagoriesMovie(movieList.id)
            .then((response) => {
                setListMovie(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }
    console.log("All Dataaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa", allData)

    const ListRateAllMovie = () => {
        showRateAllMovie(movieList.id)
            .then((response) => {
                setAllrate(response.data)
                console.log("Rate List", response.data)
            })
            .catch((err) => {
                console.error("Rate Error", err)
            })
    }


    const CalCulate = (allrate) => {
        var i = allrate
        var temp = []
        i.map(r => {
            temp.push(r.score)
        })
        console.log("iiii", temp);
        var sum = 0;
        sum = temp.reduce((previous, current) => current += previous, 0);
        let plus = sum * 2;
        avg = plus / temp.length;

    }

    console.log("AVG", avg)
    return (
        <View style={{ flex: 1, backgroundColor: '#0C0C0C' }} contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40, backgroundColor: '#FFFFFF', flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Image source={require('../logo.png')}
                            style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ flex: 1, backgroundColor: '#001F2D', marginTop: 10 }}>
                <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>{movieList.catagories_type}</Text>
                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={listMovie}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity onPress={() => props.navigation.navigate('Movie_Detail', { item, index })} >

                                <View style={{ height: 200, width: '100%', backgroundColor: '#161616', flexDirection: 'row', marginBottom: 7 }}>
                                    <View style={{ flex: 0, margin: 10, justifyContent: 'center' }}>
                                        <Image source={{ uri: item.poster }}
                                            style={{ width: 105, height: 150, borderRadius: 3 }}></Image>
                                    </View>

                                    <View style={{ flex: 1, padding: 10 }}>
                                        <View style={{ flex: 0, margin: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 14, fontWeight: 'bold' }}>{item.title}</Text>
                                        </View>

                                        <View style={{ flex: 0, margin: 5 }}>
                                            <Text style={{ color: '#BDBDBD', fontSize: 12 }}>{item.description}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                            </TouchableOpacity>

                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>


            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#001F2D'
    }
});