import React from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { Fumi } from 'react-native-textinput-effects';
import { login } from '../../api';
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { setToken } from '../../action/loginAction';

export default function Login(props) {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const dispatch = useDispatch();

    const onLogin = () => {
        login({ username, password })
            .then(response => {
                dispatch(setToken(response.data.token))
                props.navigation.navigate('Home')
            })
            .catch(error => {
                console.log("Show Error token", error.response)
                alert("incorrect")
            })

        setUsername("");
        setPassword("");


    }
    return (
        <View style={styles.container}>

            <View style={styles.userIcon}>
                <View style={styles.userIconBody}>
                    <Icon name='user' size={100} color='#000000' solid></Icon>
                </View>
            </View>

            <View style={styles.body}>
                <View style={styles.bodyTextInput}>
                    <Fumi
                        label={'Username'}
                        iconClass={Icon}
                        iconName={'user-alt'}
                        iconBackgroundColor={'#000000'}
                        iconColor={'#001F2D'}
                        iconSize={24}
                        iconWidth={40}
                        inputPadding={16}
                        inputStyle={{ color: '#001F2D' }}
                        onChangeText={(e) => setUsername(e)}
                        value={username}

                    />
                </View>

                <View style={styles.bodyTextInput}>
                    <Fumi
                        label={'Password'}
                        iconClass={Icon}
                        iconName={'lock'}
                        iconBackgroundColor={'#000000'}
                        iconColor={'#001F2D'}
                        iconSize={24}
                        iconWidth={40}
                        inputPadding={16}
                        inputStyle={{ color: '#001F2D' }}
                        secureTextEntry={true}
                        onChangeText={(e) => setPassword(e)}
                        value={password}

                    />
                </View>
            </View>

            <View style={styles.bodyButton}>
                <TouchableOpacity onPress={onLogin}>
                    <View style={styles.viewButton}>
                        <Text style={styles.textLoginButton}>Login</Text>
                    </View>
                </TouchableOpacity>

            </View>

            <View style={styles.viewFotgotANdCreat}>


                <View>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Register')}>
                        <Text style={styles.textAnother}>Create an account</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    userIcon: {
        height: 200,
        width: '100%',
        backgroundColor: '#001F2D',
        justifyContent: 'center',
        alignItems: 'center'
    },
    userIconBody: {
        height: 180,
        width: 180,
        borderRadius: 100,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center'
    },
    body: {
        justifyContent: 'center',
        margin: 20
    },
    bodyTextInput: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        borderColor: '#8A8A8A',
        borderWidth: 1,
        marginBottom: 20,
        padding: 2
    },
    bodyButton: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 70,
        marginTop: 20
    },
    viewButton: {
        height: 48,
        width: 200,
        backgroundColor: '#001F2D',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textLoginButton: {
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 18
    },
    viewFotgotANdCreat: {
        justifyContent: 'center',
        flexDirection: 'row'
    },
    textAnother: {
        color: '#ADADAD',
        fontSize: 16
    }

});