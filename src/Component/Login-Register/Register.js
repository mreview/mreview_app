import React, { useState } from 'react'
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import Icon1 from 'react-native-vector-icons/dist/MaterialIcons';
import { Fumi } from 'react-native-textinput-effects';
import { register } from '../../api';

export default function Register(props) {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [email, setEmail] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');


    const OnSubmit = () => {
        register({ username, password, email })
        setUsername("");
        setPassword("");
        setConfirmPassword("");
        setEmail("")
        props.navigation.navigate('Login')
    }

    return (
        <View style={styles.container}>

            <View style={styles.bodyTextInput}>
                <View style={styles.textInput}>
                    <Fumi
                        label={'Username'}
                        iconClass={Icon}
                        iconName={'user-alt'}
                        iconBackgroundColor={'#f2a59d'}
                        iconColor={'#001F2D'}
                        iconSize={24}
                        iconWidth={40}
                        inputPadding={16}
                        inputStyle={{ color: '#001F2D' }}
                        labelStyle={{ fontSize: 18, color: '#B8B8B8' }}
                        onChangeText={(e) => setUsername(e)}
                        value={username}
                    />
                </View>

                <View style={styles.textInput}>
                    <Fumi
                        label={'Password'}
                        iconClass={Icon}
                        iconName={'lock'}
                        type="password"
                        secureTextEntry={true}
                        iconColor={'#001F2D'}
                        iconSize={24}
                        iconWidth={40}
                        inputPadding={16}
                        inputStyle={{ color: '#001F2D' }}
                        labelStyle={{ fontSize: 18, color: '#B8B8B8' }}
                        onChangeText={(e) => setPassword(e)}
                        value={password}

                    />
                </View>

                <View style={styles.textInput}>
                    <Fumi
                        label={'Confirm Password'}
                        iconClass={Icon}
                        iconName={'unlock'}

                        secureTextEntry={true}
                        type="password"
                        iconColor={'#001F2D'}
                        iconSize={24}
                        iconWidth={40}
                        inputPadding={16}
                        inputStyle={{ color: '#001F2D' }}
                        labelStyle={{ fontSize: 18, color: '#B8B8B8' }}
                        onChangeText={(e) => setConfirmPassword(e)}
                        value={confirmPassword}
                    />
                </View>

                <View style={styles.textInput}>
                    <Fumi
                        label={'Email'}
                        iconClass={Icon1}
                        iconName={'email'}
                        iconColor={'#001F2D'}
                        iconSize={32}
                        iconWidth={40}
                        inputPadding={16}
                        inputStyle={{ color: '#001F2D' }}
                        labelStyle={{ fontSize: 18, color: '#B8B8B8' }}
                        onChangeText={(e) => setEmail(e)}
                        value={email}
                    />
                </View>

            </View>

            <View style={styles.viewBodyButton}>
                <TouchableOpacity onPress={OnSubmit}>
                    <View style={styles.viewTextButton}>
                        <Text style={styles.textButton}>Register</Text>
                    </View>
                </TouchableOpacity>

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#001F2D',
        flex: 1
    },
    bodyTextInput: {
        justifyContent: 'center',
        margin: 20,
        marginTop: 100
    },
    textInput: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        borderColor: '#8A8A8A',
        borderWidth: 1,
        marginBottom: 20,
        padding: 2
    },
    viewBodyButton: {
        justifyContent: 'center',
        alignItems: 'center',
        marginBottom: 70,
        marginTop: 20
    },
    viewTextButton: {
        height: 48,
        width: 200,
        backgroundColor: '#FFFFFF',
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 10
    },
    textButton: {
        color: '#06152C',
        fontWeight: 'bold',
        fontSize: 18
    }

});