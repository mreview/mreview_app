import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, FlatList, SafeAreaView, StyleSheet, Image, TouchableOpacity, ImageBackground, TouchableNativeFeedback, TouchableWithoutFeedback, Animated } from 'react-native'
import { Card, WingBlank } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import AppIntroSlider from 'react-native-app-intro-slider';
import { listCatagoriesMovie } from '../api';



export default function Movie(props) {

    const [catagories, setCatagories] = useState([]);

    const [comingsoon, setComingSoon] = useState([]);

    const [popular, setPopular] = useState([]);

    const [topRate, setTopRate] = useState([]);

    console.log("ComingSoon Show", comingsoon);


    const onComingSoon = () => {
        const id = 3

        listCatagoriesMovie(id)
            .then((response) => {
                setComingSoon(response.data)
                console.log(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    const onPopular = () => {
        const id = 2

        listCatagoriesMovie(id)
            .then((response) => {
                setPopular(response.data)
                console.log(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    const onTopRate = () => {
        const id = 4

        listCatagoriesMovie(id)
            .then((response) => {
                setTopRate(response.data)
                console.log(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    useEffect(() => {
        onComingSoon()
        onPopular()
        onTopRate()
    }, []);
    return (

        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }} >

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 5, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Image source={require('../logo.png')}
                        style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>

            </View>

            <View style={{ height: 40, backgroundColor: '#001F2D', flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Text style={{ color: '#9F9F9F' }}>HOME</Text>
                    </TouchableOpacity>
                </View>
                <View style={[styles.itemMenu, styles.itemSelect]}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Movie')}>
                        <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>MOVIE</Text>
                    </TouchableOpacity>

                </View>
                <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Recommend')}>
                        <Text style={{ color: '#9F9F9F' }}>RECOMMENDATIONS</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ height: 320, backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ flex: 1 }}>
                    <AppIntroSlider
                        data={comingsoon}
                        activeDotStyle={{ color: 'rgba(0 0 0 0)' }}
                        dotStyle={{ color: 'rgba(0 0 0 0)' }}
                        showNextButton={false}
                        showDoneButton={false}
                        renderItem={({ item }) => (

                            <ImageBackground source={{ uri: item.imgBg }} style={{ flex: 1 }}>

                                <View style={{ flex: 1, justifyContent: 'flex-end', alignItems: 'center' }}>
                                </View>

                                <View style={{ flex: 1, alignItems: 'flex-end', flexDirection: 'row' }}>

                                    <View style={{ backgroundColor: '#161616', height: 50, alignItems: 'flex-start', justifyContent: 'center' }}>

                                        <Image source={{ uri: item.poster }} style={{ height: 180, width: 120, marginBottom: 150, marginStart: 20 }} />

                                    </View>


                                    <View style={{ backgroundColor: '#161616', height: 50, width: 300, alignItems: 'flex-start', justifyContent: 'center' }}>
                                        <Text style={{ color: '#FFFFFF', marginEnd: 50, fontSize: 18, marginStart: 30 }}>{item.title}</Text>
                                    </View>

                                </View>


                            </ImageBackground>



                        )}
                    />



                </View>

            </View>

            <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Coming Soon</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                    </View>

                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={comingsoon}
                        horizontal={true}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity onPress={() => props.navigation.navigate('Movie_Detail', { item, index })} key={index}>
                                <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }} key={index}>
                                    <Image
                                        style={{ width: '100%', height: 208 }}
                                        source={{
                                            uri: item.poster,
                                        }}
                                    />
                                    <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                        <View style={{ flex: 1, padding: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 18, fontWeight: 'bold' }} numberOfLines={1}>{item.title}</Text>
                                        </View>
                                        <View style={{ flex: 1, padding: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                        </View>
                                    </View>
                                </Card>
                            </TouchableOpacity>

                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>

            <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Popular Movie</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>

                    </View>

                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={popular}
                        horizontal={true}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity onPress={() => props.navigation.navigate('Movie_Detail', { item, index })} key={index}>
                                <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }}>
                                    <Image
                                        style={{ width: '100%', height: 208 }}
                                        source={{
                                            uri: item.poster,
                                        }}
                                    />
                                    <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                        <View style={{ flex: 1, padding: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 18, fontWeight: 'bold' }} numberOfLines={1}>{item.title}</Text>
                                        </View>
                                        <View style={{ flex: 1, padding: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                        </View>
                                    </View>


                                </Card>
                            </TouchableOpacity>
                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>

            <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 20 }}>
                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Top Rate Movie</Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                    </View>

                </View>

                <SafeAreaView style={styles.container}>
                    <FlatList
                        data={topRate}
                        horizontal={true}
                        renderItem={({ item, index }) => (
                            <TouchableOpacity onPress={() => props.navigation.navigate('Movie_Detail', { item, index })} key={index}>
                                <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }}>

                                    <Image
                                        style={{ width: '100%', height: 208 }}
                                        source={{
                                            uri: item.poster,
                                        }}
                                    />
                                    <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                        <View style={{ flex: 1, padding: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 18, fontWeight: 'bold' }} numberOfLines={1}>{item.title}</Text>
                                        </View>
                                        <View style={{ flex: 1, padding: 5 }}>
                                            <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                        </View>
                                    </View>


                                </Card>
                            </TouchableOpacity>
                        )}
                        keyExtractor={item => item.id}
                    />
                </SafeAreaView>

            </View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#F3B502'
    },
});