import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, TouchableOpacity, StyleSheet, ImageBackground, Image, SafeAreaView, FlatList, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { Card } from '@ant-design/react-native';
import { movieDetailList, movieImages, showGenres, showRateAllMovie } from '../api';
import { useSelector } from 'react-redux';


export default function Moive_Detail(props) {

    const getMovieDetail = props.route.params.item

    console.log("Data from MovieList", getMovieDetail.id);


    const [movieDetail, setMovieDetail] = useState([]);
    const [images, setImages] = useState([]);
    const [genres, setGenres] = useState([]);
    const [rate, setRate] = useState([{}]);
    const [allrate, setAllrate] = useState([]);
    const [loading, setLoading] = useState(true);

    const token = useSelector(state => state.token)


    var avg = 0;

    useEffect(() => {
        showMovieDetail()

        showImages()
        showGenresList()
        ListRateAllMovie()

    }, []);


    const showMovieDetail = async () => {
        await movieDetailList(getMovieDetail.id)
            .then((response) => {
                setMovieDetail(response.data)
                console.log("Movie Detail", response.data)

            })
            .catch((err) => {
                console.error(err)
            })
    }

    const showImages = async () => {
        await movieImages(getMovieDetail.id)
            .then((response) => {
                setImages(response.data)
                console.log("Images", response.data)

            })
            .catch((err) => {
                console.error(err)
            })
    }

    const showGenresList = async () => {
        await showGenres(getMovieDetail.id)
            .then((response) => {
                setGenres(response.data)
                console.log("Genres Detail", response.data)
                setLoading(false)
            })
            .catch((err) => {
                console.error(err)
            })
    }


    const ListRateAllMovie = async () => {
        await showRateAllMovie(getMovieDetail.id)
            .then((response) => {
                setAllrate(response.data)
                console.log("Rate List", response.data)
                setLoading(false)

            })
            .catch((err) => {
                console.error("Rate Error", err)
            })


    }


    const CalCulate = (allrate) => {
        var i = allrate
        var temp = []

        if (temp !== undefined || temp !== null) {
            i.map(r => {
                temp.push(r.score)
            })
            console.log("iiii", temp);
            var sum = 0;
            sum = temp.reduce((previous, current) => current += previous, 0);
            let plus = sum * 2;
            avg = plus / temp.length;

        } else alert("empty")

        console.log("AVGG", avg);

    }

    const checkToken = (token) => {
        token == "" || token == null ?
            props.navigation.navigate('Login')
            :
            props.navigation.navigate('Write_Review', { item: getMovieDetail, onGoBack: () => showReviewList() })
    }



    return (
        < ScrollView style={{ flex: 1, backgroundColor: '#0C0C0C' }} contentContainerStyle={{ flexGrow: 1 }} >
            {

                loading === true ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator size="large" color="#F3B502" />
                    </View>

                    :
                    <View>
                        <View style={{ height: 40, backgroundColor: '#FFFFFF', flexDirection: 'row' }}>

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                                    <Image source={require('../logo.png')}
                                        style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                                    <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{ height: 240, width: '100%' }}>
                            <SafeAreaView style={styles.container}>
                                <FlatList
                                    data={images}
                                    horizontal={true}
                                    renderItem={({ item }) => (
                                        <View style={{ height: 300, width: 411 }}>
                                            <Image source={{ uri: item.images }} style={{ width: '100%', height: 300 }} />
                                        </View>
                                    )}
                                    keyExtractor={item => item.ID}
                                />
                                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                                    <View style={{ width: '100%', height: 64, backgroundColor: 'rgba(0,0,0,0.7)' }}>
                                        <View style={{ justifyContent: 'center' }}>
                                            <Text style={{ fontSize: 24, color: '#FFFFFF', marginStart: 20, fontWeight: 'bold' }}>{movieDetail.Title}</Text>
                                        </View>
                                        <View style={{ alignItems: 'center', flexDirection: 'row' }}>
                                            <View>
                                                <Text style={{ fontSize: 13, color: '#AEAEAE', marginStart: 20 }}>{movieDetail.Motion_Picture}</Text>
                                            </View>
                                            <View style={{ marginLeft: 5 }}>
                                                <Text style={{ color: '#FFFFFF', fontSize: 13, color: '#AEAEAE' }}>|</Text>
                                            </View>
                                            <View style={{ marginLeft: 5 }}>
                                                <Text style={{ color: '#FFFFFF', fontSize: 13, color: '#AEAEAE' }}>{movieDetail.Running_Time_Min}</Text>
                                            </View>


                                        </View>
                                    </View>
                                </View>
                            </SafeAreaView>

                        </View>
                        <View style={{ height: 180, width: '100%', backgroundColor: '#0C0C0C', flexDirection: 'row' }}>
                            <View style={{ flex: 0, margin: 10 }}>
                                <Image source={{ uri: movieDetail.ImagesPoster }}
                                    style={{ width: 105, height: 156 }}></Image>
                            </View>
                            <View style={{ flex: 1, backgroundColor: '#000000', marginBottom: 10, marginTop: 10 }}>
                                <View style={{ width: '100%', height: 30, flexDirection: 'row' }}>
                                    <FlatList
                                        data={genres}
                                        horizontal={true}
                                        renderItem={({ item }) => (
                                            <View style={{ width: 80, borderWidth: 1, borderColor: '#FFFFFF', margin: 4, alignItems: 'center' }}>
                                                <Text style={{ color: '#FFFFFF', justifyContent: 'center' }}>{item.Type}</Text>
                                            </View>
                                        )}
                                        keyExtractor={item => item.ID}
                                    />

                                </View>
                                <View style={{ flex: 1, margin: 5, padding: 2 }}>
                                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{movieDetail.Description}</Text>
                                </View>
                            </View>
                        </View>

                        {
                            movieDetail.Status === "comingsoon" ?
                                <View style={{ height: 60, width: '100%', backgroundColor: '#FFFFFF', marginBottom: 14 }}>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={{ color: '#000000', margin: 10, fontSize: 12, fontWeight: 'bold' }}>Coming Soon</Text>
                                    </View>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={{ color: '#000000', margin: 10, fontSize: 12, fontWeight: 'bold' }}>{movieDetail.Release_Data}</Text>
                                    </View>
                                </View>
                                :
                                <View style={{ height: 60, width: '100%', backgroundColor: '#FFFFFF', marginBottom: 14 }}>
                                    <View style={{ flex: 1, justifyContent: 'center' }}>
                                        <Text style={{ color: '#000000', margin: 10, fontSize: 12, fontWeight: 'bold' }}>{movieDetail.Release_Data}</Text>
                                    </View>
                                </View>

                        }


                        <View style={{ height: 100, width: '100%', backgroundColor: '#001F2D', marginBottom: 14 }}>
                            <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>

                                {/* <TouchableOpacity onPress={() => checkToken(token)} style={{ flex: 1 }}> */}
                                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                    <Icon name='star' size={24} color='#F4BC00' solid></Icon>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View onResponderStart={CalCulate(allrate)}>
                                            <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>{avg.toFixed(0)}</Text>
                                        </View>
                                        <View>
                                            <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 10</Text>
                                        </View>
                                    </View>
                                </View>

                            </View>
                        </View>

                        <View style={{ height: 100, width: '100%', backgroundColor: '#001F2D', marginBottom: 14 }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 16, fontWeight: 'bold' }}>User Review</Text>
                                </View>

                                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                                    <TouchableOpacity onPress={() => props.navigation.navigate('User_Review', { movieDetail })}>
                                        <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                                    </TouchableOpacity>

                                </View>
                            </View>

                        </View>

                        <View style={{ height: 280, width: '100%', backgroundColor: '#001F2D', marginBottom: 14 }}>
                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 16, fontWeight: 'bold' }}>Image</Text>
                                </View>

                                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>
                                    <TouchableOpacity onPress={() => props.navigation.navigate('Photo_List', { movieDetail })}>
                                        <Text style={{ color: '#F3B502', margin: 10, fontSize: 15, fontWeight: 'bold' }}>SEE ALL</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <View style={{ height: 200, width: '100%' }}>
                                <SafeAreaView style={styles.container}>
                                    <FlatList
                                        data={images}
                                        horizontal={true}
                                        renderItem={({ item, index }) => (
                                            <TouchableOpacity onPress={() => props.navigation.navigate('Photo_Show', { item, index })}>
                                                <Card style={{ width: 317, height: 180, marginLeft: 10, marginRight: 10, }}>
                                                    <Image
                                                        style={{ width: '100%', height: 179 }}
                                                        source={{
                                                            uri: item.images,
                                                        }}
                                                    />
                                                </Card>
                                            </TouchableOpacity>

                                        )}
                                        keyExtractor={item => item.id}
                                    />
                                </SafeAreaView>

                            </View>

                        </View>
                    </View>

            }

        </ScrollView >
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#001F2D'
    }
});

