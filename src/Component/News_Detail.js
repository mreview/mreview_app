import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Image, ScrollView, Linking } from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { listAllNews } from '../api';

export default function News_Detail(props) {

    const newRes = props.route.params.item

    const [newDetail, setNewDetail] = useState([]);

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 14, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Image source={require('../logo.png')}
                            style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ height: 122, backgroundColor: '#001F2D', marginBottom: 14 }}>
                <View style={{ flex: 1, padding: 10 }}>
                    <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>{newRes.header_news}</Text>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 10 }}>
                    <Text style={{ color: '#A4A4A4', fontSize: 12 }}>BY {newRes.create_by}</Text>
                    <Text style={{ color: '#A4A4A4', fontSize: 12 }}>{newRes.date}</Text>
                </View>
            </View>

            <View style={{ height: 225, backgroundColor: '#001F2D', marginBottom: 14, padding: 10 }}>
                <Image style={{ width: '100%', height: 200, }} source={{ uri: newRes.image, }} />
            </View>

            <View style={{ flex: 1, backgroundColor: '#001F2D', marginBottom: 14, padding: 10 }}>

                <View style={{ marginBottom: 5 }}>
                    <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{newRes.detail}</Text>
                </View>
            </View>
        </ScrollView>

    )
}