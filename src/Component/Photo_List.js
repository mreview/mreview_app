import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, TouchableOpacity, Image, FlatList } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { movieImages } from '../api';


export default function Photo_List(props) {


    const getID = props.route.params.movieDetail.ID

    const [showMovieImages, setshowMovieImages] = useState([]);

    console.log("ID from Detail", getID);

    const showListImages = () => {
        movieImages(getID)
            .then((response) => {
                setshowMovieImages(response.data)
                console.log("Movie Photo", response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    useEffect(() => {
        showListImages()
    }, []);


    return (
        <View style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 14, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Image source={require('../logo.png')}
                            style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ flex: 1, backgroundColor: '#001F2D' }}>
                <View style={{ flexDirection: 'row', flex: 1, marginBottom: 5 }}>
                    <FlatList
                        data={showMovieImages}
                        numColumns={2}
                        renderItem={({ item, index }) => (
                            <View style={{ flex: 1, width: 137 }} key={index}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Photo_Show', { item })}>
                                    <Image style={{ width: '100%', height: 100, }} source={{ uri: item.images }} />
                                </TouchableOpacity>
                            </View>
                        )}
                        keyExtractor={item => item.ID}
                    />

                </View>



            </View>
        </View>
    )
}