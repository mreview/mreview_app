import React, { useRef, useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, StyleSheet, ScrollView, SafeAreaView, FlatList, Image, Dimensions, } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { Card } from '@ant-design/react-native';
import Carousel from 'react-native-anchor-carousel';
import { listCatagories, listAllMovie, listCatagoriesMovie, showRateReview } from '../api';





export default function Recommend(props) {

    const [catagories, setCatagories] = useState([]);
    const [recommendList, setRecommendList] = useState([]);
    const [getID, setGetID] = useState({});




    const carouselRef = useRef(null);

    const { width, height } = Dimensions.get('window');

    const [background, setBackground] = useState({

        uri: "ttps://m.media-amazon.com/images/M/MV5BZmUwNGU2ZmItMmRiNC00MjhlLTg5YWUtODMyNzkxODYzMmZlXkEyXkFqcGdeQXVyNTIzOTk5ODM@._V1_SY1000_SX750_AL_.jpg",
        name: "Batman Begins (2005)",
        desc: 'After training with his mentor, Batman begins his fight to free crime-ridden Gotham City from corruption.',

    })

    const [movie, setMovie] = useState([]);

    useEffect(() => {
        onListCatagories()
        onRecommended()
    }, []);

    const onListCatagories = () => {
        listCatagories()
            .then((response) => {
                setCatagories(response.data)
                console.log(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    const onRecommended = () => {
        const id = 1

        listCatagoriesMovie(id)
            .then((response) => {
                setRecommendList(response.data)
                console.log(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }



    const Movie = ({ item, index }) => {
        return (
            <View>
                <TouchableOpacity onPress={() => {
                    carouselRef.current.scrollToIndex(index);
                    setBackground({
                        uri: item.poster,
                        name: item.title,
                        desc: item.description,
                    })
                        , setGetID(item)
                }}>
                    <Image source={{ uri: item.poster }} style={styles.carouselImage} />
                    <View style={{ marginTop: 10, padding: 5, position: 'relative' }}>
                        <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 16, fontWeight: 'bold' }} numberOfLines={1}>{item.title}</Text>
                    </View>
                </TouchableOpacity>
            </View>
        )
    }

    return (

        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }} >

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 5, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Image source={require('../logo.png')}
                            style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>

            </View>

            <View style={{ height: 40, backgroundColor: '#001F2D', flexDirection: 'row', marginBottom: 10 }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Text style={{ color: '#9F9F9F' }}>HOME</Text>
                    </TouchableOpacity>
                </View>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Movie')}>
                        <Text style={{ color: '#9F9F9F' }}>MOVIE</Text>
                    </TouchableOpacity>

                </View>
                <View style={[styles.itemMenuRecommend, styles.itemSelect]}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Recommend')}>
                        <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>RECOMMENDATIONS</Text>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ height: 550, width: '100%', backgroundColor: '#001F2D', marginBottom: 10 }}>
                <View style={{ justifyContent: 'center' }}>
                    <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Recommended For You</Text>
                </View>



                <View style={styles.carouselContainerView}>
                    <Carousel
                        style={styles.Carousel}
                        data={recommendList}
                        renderItem={Movie}
                        itemWidth={200}
                        containerWidth={width - 20}
                        separatorWidth={0}
                        ref={carouselRef}
                        inActiveOpacity={0.4}
                    />
                </View>

                <View style={styles.movieInfoContainer}>
                    <View style={{ justifyContent: 'center', flex: 1 }}>
                        <Text style={styles.movieName} numberOfLines={1}>{background.name}</Text>
                    </View>
                </View>
                <View style={{ paddingHorizontal: 14, marginTop: 14 }}>
                    <Text style={styles.movieDis} numberOfLines={4}>{background.desc}</Text>
                </View>



            </View>

            <View style={{ backgroundColor: '#001F2D', width: '100%', flex: 1, marginBottom: 10 }}>
                <View style={{ justifyContent: 'center', marginBottom: 5 }}>
                    <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Catagories</Text>
                </View>
                {
                    catagories.map((item, index) => {
                        return (
                            <TouchableOpacity onPress={() => props.navigation.navigate('Movie_List', { item, index })} style={{ marginBottom: 10 }}>
                                <View style={{ backgroundColor: 'rgba(0,0,0,0.5)', opacity: 42, width: '100%', height: 55, alignItems: 'center', flexDirection: 'row' }}>

                                    <View style={{ flex: 1 }}>
                                        <Text style={{ color: '#FFFFFF', marginStart: 20, fontSize: 18 }}>{item.catagories_type}</Text>
                                    </View>
                                    <View style={{ flex: 1, alignItems: 'flex-end', marginEnd: 40 }}>
                                        <Icon name='chevron-right' size={20} color='#FFFFFF'></Icon>
                                    </View>

                                </View>
                            </TouchableOpacity>

                        )
                    })
                }
            </View>

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemMenuRecommend: {
        flex: 1.5,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#F3B502'
    },
    cardItem: {
        width: 150,
        height: 263,
        marginLeft: 10,
        marginRight: 10,
    },
    cardItemSelect: {
        borderBottomWidth: 5,
        borderColor: '#023A54'
    },
    cardItemTital: {
        height: 50,
        alignItems: 'center',
        backgroundColor: '#161616'
    },
    carouselImage: {

    },
    carouselContainerView: {
        width: '100%',
        height: 350,
        justifyContent: 'center',
        alignItems: 'center'
    },
    Carousel: {
        flex: 1,
        overflow: 'visible'
    },
    carouselImage: {
        width: 200,
        height: 320,
        borderRadius: 10,
        alignSelf: 'center',
        backgroundColor: 'rgba(0,0,0,0.9)'
    },
    movieInfoContainer: {
        flexDirection: 'row',
        marginTop: 16,
        justifyContent: 'space-between',
        width: Dimensions.get('window').width - 14
    },
    movieName: {
        paddingLeft: 14,
        color: '#FFFFFF',
        fontWeight: 'bold',
        fontSize: 20,
        marginBottom: 6
    },
    movieDis: {
        color: '#FFFFFF',
        opacity: 0.8,
        lineHeight: 20,
        fontSize: 12
    }
});