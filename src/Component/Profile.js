import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { useSelector } from 'react-redux';

export default function Profile(props) {

    const token = useSelector(state => state.token)

    console.log(token);

    return (
        <View style={styles.container}>
            <View style={styles.viewHeader}>

                <View style={styles.viewLogo}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Image source={require('../logo.png')}
                            style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                    </TouchableOpacity>

                </View>

                <View style={styles.viewProfile}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>

            </View>


            {
                token == "" || token == null ?
                    <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
                        <View style={styles.viewList}>
                            <View style={styles.viewIconList}>
                                <Icon name='sign-in-alt' size={32} color='#4FF75E' solid></Icon>
                            </View>

                            <View style={styles.viewTextList}>
                                <Text style={styles.TextList}>Login</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                    :
                    <View>

                        <TouchableOpacity onPress={() => props.navigation.navigate('Login')}>
                            <View style={styles.viewList}>
                                <View style={styles.viewIconList}>
                                    <Icon name='sign-in-alt' size={32} color='#E52525' solid></Icon>
                                </View>

                                <View style={styles.viewTextList}>
                                    <Text style={styles.TextList}>Logout</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>

            }

        </View >
    )
}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#161616',
        flex: 1
    },
    viewHeader: {
        height: 40,
        backgroundColor: '#FFFFFF',
        marginBottom: 10,
        flexDirection: 'row'
    },
    viewLogo: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-start',
        marginStart: 20
    },
    viewProfile: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'flex-end',
        marginEnd: 20
    },
    viewList: {
        width: '100%',
        height: 56,
        backgroundColor: '#001F2D',
        marginBottom: 5,
        flexDirection: 'row'
    },
    viewIconList: {
        flex: 0,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 30
    },
    viewTextList: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center'
    },
    TextList: {
        color: '#FFFFFF',
        fontSize: 16,
        fontWeight: 'bold'
    },
    viewButton: {
        flex: 1,
        margin: 50,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    button: {
        width: '100%'
    },
    viewBodyButtonLogin: {
        width: '80%',
        height: 64,
        backgroundColor: '#4FF75E',
        borderRadius: 20,
        marginBottom: 5,
        flexDirection: 'row',
    },
    viewBodyButtonLogout: {
        width: '80%',
        height: 64,
        backgroundColor: '#E52525',
        borderRadius: 20,
        marginBottom: 5,
        flexDirection: 'row',
    },
    viewBodyIconButton: {
        flex: 1,
        alignContent: 'center',
        justifyContent: 'center',
        padding: 30,
    },
    textButton: {
        flex: 3,
        alignContent: 'center',
        justifyContent: 'center'
    }

});