import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, FlatList, SafeAreaView, StyleSheet, Image, TouchableOpacity, ImageBackground, TouchableNativeFeedback, TouchableWithoutFeedback, Animated, ActivityIndicator } from 'react-native'
import { Card, WingBlank } from '@ant-design/react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import AppIntroSlider from 'react-native-app-intro-slider';
import axios from 'axios';
import { listAllNews, listAllMovie } from '../api';


export default function Home(props) {

    const [news, setNews] = useState([]);
    const [movie, setMovie] = useState([]);

    const [loading, setLoading] = useState(true);

    const showlistNews = () => {
        listAllNews()
            .then((response) => {
                setNews(response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    const showListMovie = () => {
        listAllMovie()
            .then((response) => {
                setMovie(response.data)
                console.log(response.data)
                setLoading(false)
            })
            .catch((err) => {
                console.error(err)
            })

    }

    useEffect(() => {
        showlistNews()
        showListMovie()
    }, []);


    return (

        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }} >
            {
                loading === true ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                    :
                    <View>
                        <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 5, flexDirection: 'row' }}>

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                                    <Image source={require('../logo.png')}
                                        style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                                </TouchableOpacity>
                            </View>

                            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                                    <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                                </TouchableOpacity>
                            </View>

                        </View>

                        <View style={{ height: 40, backgroundColor: '#001F2D', flexDirection: 'row', marginBottom: 10 }}>
                            <View style={[styles.itemMenu, styles.itemSelect]}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                                    <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>HOME</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Movie')}>
                                    <Text style={{ color: '#9F9F9F' }}>MOVIE</Text>
                                </TouchableOpacity>

                            </View>
                            <View style={{ flex: 1.5, alignItems: 'center', justifyContent: 'center' }}>
                                <TouchableOpacity onPress={() => props.navigation.navigate('Recommend')}>
                                    <Text style={{ color: '#9F9F9F' }}>RECOMMENDATIONS</Text>
                                </TouchableOpacity>
                            </View>
                        </View>

                        <View style={{ flex: 1 }}>
                            {
                                news.map((item, index) => {
                                    return (
                                        <View style={{ width: '100%', height: 400, backgroundColor: '#001F2D', marginBottom: 10 }} key={index}>
                                            <View style={{ width: '100%', height: 64, padding: 10 }}>
                                                <Text style={{ fontSize: 16, color: '#FFFFFF', fontWeight: 'bold' }}>{item.header_news}</Text>
                                            </View>

                                            <View style={{ width: '100%', height: 200, paddingLeft: 10, paddingRight: 10 }}>
                                                <Image style={{ width: '100%', height: 200 }} source={{ uri: item.image, }} key={index} />
                                            </View>
                                            <View style={{ width: '100%', height: 48, padding: 10 }}>
                                                <Text style={{ color: '#FFFFFF', fontSize: 12 }}>{item.shot_detail}</Text>
                                            </View>

                                            <View style={{ flex: 1, justifyContent: 'center', padding: 10 }}>
                                                <TouchableOpacity onPress={() => props.navigation.navigate('News_Detail', { item, index })}>
                                                    <Text style={{ color: '#F3B502', fontSize: 14, fontWeight: 'bold' }}> SEE MORE</Text>
                                                </TouchableOpacity>
                                            </View>

                                        </View>
                                    )
                                })
                            }

                        </View>



                        <View style={{ height: 350, backgroundColor: '#001F2D', marginBottom: 20 }}>
                            <View style={{ flexDirection: 'row' }}>
                                <View style={{ flex: 1, justifyContent: 'center' }}>
                                    <Text style={{ color: '#FFFFFF', margin: 10, fontSize: 18, fontWeight: 'bold' }}>Movie</Text>
                                </View>
                                <View style={{ flex: 1, alignItems: 'flex-end', justifyContent: 'center', marginEnd: 20 }}>

                                </View>

                            </View>

                            <SafeAreaView style={styles.container}>
                                <FlatList
                                    data={movie}
                                    horizontal={true}
                                    renderItem={({ item, index }) => (
                                        <TouchableOpacity onPress={() => props.navigation.navigate('Movie_Detail', { item, index })} key={index}>
                                            <Card style={{ width: 180, height: 280, marginLeft: 10, marginRight: 10, }}>

                                                <Image
                                                    style={{ width: '100%', height: 208 }}
                                                    source={{
                                                        uri: item.images_poster,
                                                    }}
                                                />
                                                <View style={{ height: 70, paddingLeft: 10, backgroundColor: '#161616' }}>
                                                    <View style={{ flex: 1, padding: 5 }}>
                                                        <Text style={{ color: '#FFFFFF', fontSize: 16, fontWeight: 'bold' }}>{item.title}</Text>
                                                    </View>
                                                    <View style={{ flex: 1, padding: 5 }}>
                                                        <Text style={{ color: '#FFFFFF', fontSize: 12, }}>{item.date}</Text>
                                                    </View>
                                                </View>


                                            </Card>
                                        </TouchableOpacity>

                                    )}
                                    keyExtractor={item => item.index}
                                />
                            </SafeAreaView>

                        </View>
                    </View>

            }

        </ScrollView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    itemMenu: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemSelect: {
        borderBottomWidth: 5,
        borderColor: '#F3B502'
    },
    flatListStyles: {
        flex: 1,
        backgroundColor: "#001F2D",
        padding: 20
    }
});