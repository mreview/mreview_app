import React, { useState, useEffect } from 'react'
import { View, Text, TouchableOpacity, Image } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { GetOneImages } from '../api';

export default function Photo_Show(props) {

    const [image, setImage] = useState([]);
    const getID = props.route.params.item.ID

    const ShowOneImage = () => {
        GetOneImages(getID)
            .then((response) => {
                setImage(response.data)
                console.log("Movie Photo", response.data)
            })
            .catch((err) => {
                console.error(err)
            })
    }

    useEffect(() => {
        ShowOneImage()
    }, []);


    return (
        <View style={{ flex: 1, backgroundColor: '#161616' }}>

            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 14, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Home')}>
                        <Image source={require('../logo.png')}
                            style={{ width: 32, height: 32, borderRadius: 10 }}></Image>
                    </TouchableOpacity>
                </View>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', marginEnd: 20 }}>
                    <TouchableOpacity onPress={() => props.navigation.navigate('Profile')}>
                        <Icon name='user-circle' size={24} color='#F3B502' solid></Icon>
                    </TouchableOpacity>
                </View>
            </View>

            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 10 }}>
                <Image style={{ width: '100%', height: 210, }} source={{ uri: image.images, }} />
            </View>


        </View>
    )
}