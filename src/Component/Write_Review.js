import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView, ImageBackground, Image, TextInput, ActivityIndicator } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { TouchableOpacity } from 'react-native-gesture-handler';
import SelectInput from 'react-native-select-input-ios';
import { AirbnbRating } from 'react-native-ratings';
import { movieDetailList, CreateReview, listGenres, CreateRate, showRateAllMovie } from '../api';

import { useSelector } from 'react-redux';
import { IndexPath, Layout, Select, SelectItem } from '@ui-kitten/components';

export default function Write_Review(props) {

    const [header, setHeader] = useState("");
    const [detail, setDetail] = useState("");
    const [movie, setMovie] = useState([""]);
    const [rateType, setRateType] = useState();
    const [rate_score, setRateScore] = useState();
    const [genres, setGenres] = useState([]);
    const [allrate, setAllrate] = useState([]);
    const [showRate, setShowRate] = useState(0);
    const [loading, setLoading] = useState(true);
    var avg = 0;

    const renderOption = title => <SelectItem title={title} />;
    const data = genres;
    const [selectedIndex, setSelectedIndex] = useState(new IndexPath(0));
    const rate_type = data[selectedIndex.row];




    const [counteries, setCounteries] = useState([]);

    const ratingCompleted = (rating) => {
        setRateScore(rating)
        console.log("Rating is: " + rating)
    }





    const MovieDetail = props.route.params.item
    const token = useSelector(state => state.token)
    const [test, setTest] = useState(false);






    useEffect(() => {

        ShowMovie()
        ListAllGenres()
        ListRateAllMovie()

    }, []);

    const ShowMovie = () => {
        movieDetailList(MovieDetail.ID)
            .then((response) => {
                setMovie(response.data)
                console.log("Review List", response.data)
            })
            .catch((err) => {
                console.error("Review error", err)
            })
    }

    const ListAllGenres = () => {
        listGenres()
            .then((response) => {
                const temp = []
                response.data.map(r => {
                    temp.push(r.gen_type)
                })
                setGenres(temp)
                console.log("Gennnn", response.data)
            })
            .catch((err) => {
                console.error("Gen error", err)
            })
    }

    const ListRateAllMovie = () => {
        showRateAllMovie(MovieDetail.ID)
            .then((response) => {
                setAllrate(response.data)
                console.log("Rate List", response.data)
                setLoading(false)
            })
            .catch((err) => {
                console.error("Rate Error", err)
            })
    }

    const CalCulate = (allrate) => {
        var i = allrate
        var temp = []
        i.map(r => {
            temp.push(r.score)
        })
        console.log("iiii", temp);
        var sum = 0;
        sum = temp.reduce((previous, current) => current += previous, 0);
        let plus = sum * 2;
        avg = plus / temp.length;

    }

    const onCreate = (event) => {

        event && event.preventDefault && event.preventDefault() // For fix double tab (TouchableOpacity)

        CreateReview(MovieDetail.ID, { review_header: header, review_text: detail }, token)
            .then((response) => {
                CreateRate(response.data.id, rate_type, rate_score, token)
                    .then((response) => {
                        props.navigation.navigate('User_Review')
                        props.route.params.onGoBack()
                    })
                    .catch((err) => {
                        console.error("Create Error", err)
                    })
            })
            .catch((err) => {
                console.error("Create Error", err)
            })
    }



    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>
            {
                loading === true ?
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        <ActivityIndicator size="large" color="#0000ff" />
                    </View>
                    :
                    <View>
                        <ImageBackground blurRadius={7} source={{ uri: movie.ImagesPoster }} style={{ flex: 1 }}>
                            <View style={{ backgroundColor: 'rgba(0, 31, 46, 0.89)', flex: 1 }}>

                                <View style={{ justifyContent: 'flex-start', alignItems: 'flex-start', marginStart: 20, marginTop: 30 }}>
                                    <TouchableOpacity onPress={() => props.navigation.navigate('User_Review')}>
                                        <Icon name='times-circle' size={24} color='#FFFFFF' solid></Icon>
                                    </TouchableOpacity>
                                </View>

                                <View style={{ flex: 1, width: '100%', alignItems: 'center', marginBottom: 10 }}>
                                    <Image
                                        style={{ width: 200, height: 300 }}
                                        source={{
                                            uri: movie.ImagesPoster,
                                        }}
                                    />
                                </View>

                                <View style={{ flex: 1, marginBottom: 15, marginTop: 10 }}>
                                    <View style={{ justifyContent: 'flex-start', alignItems: 'center' }}>
                                        <Text style={{ fontSize: 24, color: '#FFFFFF', fontWeight: 'bold' }}>Total Rating</Text>
                                        <View style={{ flexDirection: 'row', marginTop: 10 }}>
                                            <View onResponderStart={() => CalCulate(allrate)}>

                                                <Text style={{ fontSize: 32, color: '#F3B502', fontWeight: 'bold' }}>{avg.toFixed(0)}</Text>

                                            </View>
                                            <View>
                                                <Text style={{ fontSize: 32, color: '#FFFFFF' }}> / 10</Text>
                                            </View>
                                        </View>
                                    </View>

                                </View>

                                <View style={{ flex: 1 }}>
                                    <View style={{ marginStart: 20 }}>
                                        <Text style={{ fontSize: 18, color: '#FFFFFF', fontWeight: 'bold' }}>Your Rating</Text>
                                    </View>

                                    <View style={{ flex: 1, flexDirection: 'row' }}>
                                        <View style={{ flex: 1 }}>

                                            <Select
                                                style={{ backgroundColor: '#FFFFFF', borderWidth: 1, borderColor: 'black', overflow: 'hidden', width: '70%', marginTop: 40, marginStart: 20 }}
                                                placeholder="Please selete sex"
                                                value={rate_type}
                                                selectedIndex={selectedIndex}
                                                onSelect={index => setSelectedIndex(index)}>
                                                {data.map(renderOption)}
                                            </Select>
                                        </View>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ alignItems: 'center' }}>
                                                <AirbnbRating
                                                    count={5}
                                                    reviews={false}
                                                    defaultRating={0}
                                                    size={24}
                                                    onFinishRating={ratingCompleted}
                                                />
                                            </View>
                                        </View>
                                    </View>



                                </View>

                                <View style={{ flex: 1 }}>
                                    <View style={{ marginStart: 20 }}>
                                        <Text style={{ fontSize: 18, color: '#FFFFFF', fontWeight: 'bold' }}>Your Review</Text>
                                    </View>

                                    <View style={{ flex: 1 }}>
                                        <View style={{ margin: 10 }}>
                                            <TextInput
                                                placeholder='Write a Header for your review'
                                                placeholderTextColor='#595959'
                                                onChangeText={(value) => setHeader(value)}
                                                value={header}
                                                style={{
                                                    backgroundColor: '#FFFFFF',
                                                    borderBottomColor: '#000000',
                                                    borderBottomWidth: 1,

                                                }} />
                                        </View>

                                        <View style={{ margin: 10 }}>
                                            <TextInput
                                                maxLength={1000}
                                                multiline={true}
                                                placeholder='Write your review here'
                                                placeholderTextColor='#595959'
                                                onChangeText={(value) => setDetail(value)}
                                                value={detail}
                                                style={{
                                                    alignItems: 'flex-start',
                                                    height: 120,
                                                    backgroundColor: '#FFFFFF',
                                                    borderBottomColor: '#000000',
                                                    borderBottomWidth: 1,


                                                }} />
                                        </View>

                                    </View>
                                </View>

                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', margin: 25 }}>
                                    <TouchableOpacity onPress={(e) => onCreate(e)}>
                                        <Icon name='telegram' size={56} color='#FFFFFF' solid></Icon>
                                    </TouchableOpacity>
                                </View>

                            </View>
                        </ImageBackground>
                    </View>
            }


        </ScrollView >


    )
}