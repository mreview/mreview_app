import React, { useState, useEffect } from 'react'
import { View, Text, ScrollView } from 'react-native'
import Icon from 'react-native-vector-icons/dist/FontAwesome5';
import { TouchableOpacity, FlatList } from 'react-native-gesture-handler';
import { showReview, haveUserReview, showRateReview } from '../api';
import ActionButton from 'react-native-action-button';
import { useSelector } from 'react-redux';
import moment from 'moment';

export default function User_Review(props) {

    const [review, setReview] = useState([]);

    const getMovieDetail = props.route.params.movieDetail
    const token = useSelector(state => state.token)
    console.log(token);
    console.log("Reviewwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww", review);



    useEffect(() => {
        showReviewList()
    }, []);

    const checkToken = (token) => {
        token == "" || token == null ?
            props.navigation.navigate('Login')
            :
            props.navigation.navigate('Write_Review', { item: getMovieDetail, onGoBack: () => showReviewList() })
    }

    const showReviewList = () => {
        haveUserReview(getMovieDetail.ID)
            .then((response) => {
                setReview(response.data)
                console.log("Review List", response.data)
            })
            .catch((err) => {
                console.error("Review error", err)
            })
    }


    return (
        <View style={{ flex: 1, backgroundColor: '#161616' }} contentContainerStyle={{ flexGrow: 1 }}>
            <View style={{ height: 40, backgroundColor: '#FFFFFF', marginBottom: 10, flexDirection: 'row' }}>

                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-start', marginStart: 20 }}>
                    <Text style={{ fontSize: 18, fontWeight: 'bold' }}>User Review</Text>
                </View>

            </View>

            <FlatList
                data={review}
                renderItem={({ item, index }) => (
                    <View style={{ backgroundColor: '#001F2D', width: '100%', height: 200, marginBottom: 10 }} key={index}>

                        <View style={{ flexDirection: 'row', }}>
                            <View style={{ padding: 10, marginStart: 7 }}>
                                <Icon name='user-circle' size={24} color='#FFFFFF' solid></Icon>
                            </View>

                            <View style={{ padding: 10 }}>
                                <Text style={{ color: '#FFFFFF', fontSize: 14, fontWeight: 'bold' }}>{item.user_name}</Text>
                            </View>


                            <View style={{ flex: 1, padding: 10, marginEnd: 20, alignItems: 'flex-end' }}>
                                <Text style={{ color: '#AEAEAE' }}>{moment(item.CreatedAt).format('LLLL')}</Text>
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                                <Icon name='star' size={16} color='#F4BC00' solid></Icon>
                            </View>

                            <View style={{ flex: 0, alignItems: 'flex-start', justifyContent: 'center', marginStart: 5, marginEnd: 10 }}>
                                <View style={{ flexDirection: 'row' }}>
                                    <View>
                                        <Text style={{ fontSize: 14, color: '#F3B502', fontWeight: 'bold' }}>{item.score}</Text>
                                    </View>
                                    <View>
                                        <Text style={{ fontSize: 14, color: '#FFFFFF' }}> / 5</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ flex: 1, alignItems: 'flex-start', justifyContent: 'center', marginStart: 20, marginEnd: 10 }}>
                                <Text style={{ fontSize: 14, color: '#FFFFFF', fontWeight: 'bold' }}>{item.header}</Text>
                            </View>

                            <View style={{ width: 78, height: 30, backgroundColor: '#FFFFFF', margin: 4, alignItems: 'center', justifyContent: 'center', borderRadius: 8, marginRight: 15 }}>
                                <Text style={{ color: '#000000', justifyContent: 'center', fontSize: 14, fontWeight: 'bold' }}>{item.rate_type}</Text>
                            </View>
                        </View>

                        <View style={{ flex: 1, padding: 15 }}>
                            <Text style={{ color: '#FFFFFF' }} numberOfLines={6}>
                                {item.detail}
                            </Text>
                        </View>

                    </View>
                )}
                keyExtractor={item => item.index}
            />



            <ActionButton style={{ position: 'absolute' }} onPress={() => checkToken(token)} buttonColor="#F4BC00" position='right' />
        </View>

    )
}
